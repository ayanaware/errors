import * as errors from './errors';

export function isGenericError(error: Error): error is errors.GenericError {
	return error instanceof errors.GenericError;
}

export function isAbortError(error: Error): error is errors.AbortError {
	return error instanceof errors.AbortError;
}

export function isFileSystemError(error: Error): error is errors.FileSystemError {
	return error instanceof errors.FileSystemError;
}

export function isIllegalAccessError(error: Error): error is errors.IllegalAccessError {
	return error instanceof errors.IllegalAccessError;
}

export function isIllegalArgumentError(error: Error): error is errors.IllegalArgumentError {
	return error instanceof errors.IllegalArgumentError;
}

export function isIllegalStateError(error: Error): error is errors.IllegalStateError {
	return error instanceof errors.IllegalStateError;
}

export function isIOError(error: Error): error is errors.IOError {
	return error instanceof errors.IOError;
}

export function isLocalizedError(error: Error): error is errors.LocalizedError {
	return error instanceof errors.LocalizedError;
}

export function isMethodNotImplementedError(error: Error): error is errors.MethodNotImplementedError {
	return error instanceof errors.MethodNotImplementedError;
}

export function isNetworkError(error: Error): error is errors.NetworkError {
	return error instanceof errors.NetworkError;
}

export function isParseError(error: Error): error is errors.ParseError {
	return error instanceof errors.ParseError;
}

export function isProcessingError(error: Error): error is errors.ProcessingError {
	return error instanceof errors.ProcessingError;
}

export function isTimeoutError(error: Error): error is errors.TimeoutError {
	return error instanceof errors.TimeoutError;
}

export function isValidationError(error: Error): error is errors.ValidationError {
	return error instanceof errors.ValidationError;
}
