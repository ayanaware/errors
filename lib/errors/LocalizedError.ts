import { GenericError } from './GenericError';

/**
 * Error used when intending to show a localized message to a user.
 */
export class LocalizedError extends GenericError {
	public readonly key!: string;
	public readonly interpolationArgs!: { [key: string]: string };

	public constructor(message: { key: string, interpolationArgs?: { [key: string]: string } });
	public constructor(key: string, interpolationArgs?: { [key: string]: string });
	public constructor() {
		let key;
		let interpolationArgs;
		if (typeof arguments[0] === 'string') {
			key = arguments[0];
			interpolationArgs = arguments[1];
		} else {
			key = arguments[0].key;
			interpolationArgs = arguments[0].args;
		}

		super(key);

		this.__define('key', key);
		this.__define('interpolationArgs', interpolationArgs);
	}
}
