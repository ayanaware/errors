import { GenericError } from './GenericError';

/**
 * Error used when an invalid argument has been passed.
 */
export class IllegalArgumentError extends GenericError {}
