import { ProcessingError } from './ProcessingError';

/**
 * Error used when parsing of anything goes wrong.
 */
export class ParseError extends ProcessingError {}
