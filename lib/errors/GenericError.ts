import { inspect } from 'util';

/**
 * Overridden Error class for the documentation.
 *
 * @ignore
 */
declare class Error {
	/**
	 * @ignore
	 *
	 * @param message The message
	 */
	public constructor(message?: string);

	/**
	 * The name of this error. This is usually equal to the class name of the error.
	 */
	public readonly name: string;

	/**
	 * The message of this error.
	 */
	public readonly message: string;

	/**
	 * The stack of this error.
	 */
	public readonly stack?: string;
}

/**
 * The base class for every Error.
 * This class should never be directly created or thrown.
 */
export class GenericError extends Error {
	private readonly code!: number;
	private readonly cause!: Error;

	/**
	 * Creates a new instance with an optional message.
	 *
	 * @param msg An optional message for this error
	 */
	public constructor(msg: string = null) {
		super(msg);

		Object.defineProperty(this, 'name', {
			writable: false,
			configurable: false,
			enumerable: false,
			value: this.constructor.name,
		});
	}

	/**
	 * Sets the cause of this error.
	 *
	 * @param cause Another Error to be set as cause
	 *
	 * @returns The current error for method chaining
	 */
	public setCause(cause: Error): this {
		this.__define('cause', cause);

		return this;
	}

	/**
	 * Returns the cause of this error.
	 *
	 * @returns The cause of this error
	 */
	public getCause() {
		return this.cause;
	}

	/**
	 * Sets the code of this error.
	 *
	 * @param code A code to be set
	 *
	 * @returns The current error for method chaining
	 */
	public setCode(code: number): this {
		this.__define('code', code);

		return this;
	}

	/**
	 * Returns the code of this error.
	 *
	 * @returns The code of this error
	 */
	public getCode() {
		return this.code;
	}

	/**
	 * Whether this error has a code or not.
	 *
	 * @returns true if this error has a code, false if otherwise
	 */
	public hasCode() {
		return typeof this.code === 'number';
	}

	/**
	 * Returns the full stack of this error.
	 * This also includes the cause of this error and all further causes.
	 *
	 * @returns The full stack of this error
	 */
	public getFullStack() {
		let stringifiedCause = '';
		if (this.cause != null) {
			stringifiedCause = '\nCaused by: ';

			if (this.cause instanceof GenericError) {
				stringifiedCause += this.cause.getFullStack();
			} else if (typeof this.cause === 'object' && this.cause.stack) {
				stringifiedCause += this.cause.stack;
			} else {
				stringifiedCause += this.cause;
			}
		}

		let stack = this.stack;
		if (this.hasCode()) {
			stack = stack.replace(':', `: (${this.code})`);
		}

		return `${stack}${stringifiedCause}`;
	}

	/**
	 * Custom inspect function for proper formatting with simple console.log statements.
	 *
	 * @ignore
	 *
	 * @returns The full stack of this error
	 */
	public [inspect.custom]() {
		return this.getFullStack();
	}

	/**
	 * Defines a non-writable and non-enumerable property on this error.
	 * This function can be used by subclasses to easily attach properties.
	 *
	 * @param propertyKey The property key of the property to define
	 * @param value The value for the property
	 */
	protected __define(propertyKey: string, value: any) {
		Object.defineProperty(this, propertyKey, {
			writable: false,
			configurable: true,
			enumerable: false,
			value,
		});
	}
}
