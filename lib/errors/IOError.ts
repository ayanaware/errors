import { GenericError } from './GenericError';

/**
 * Error used when an IO operation of any kind goes wrong.
 */
export class IOError extends GenericError {}
