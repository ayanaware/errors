import { ProcessingError } from './ProcessingError';

/**
 * Error used when a process times out.
 */
export class TimeoutError extends ProcessingError {}
