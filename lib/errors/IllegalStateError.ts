import { GenericError } from './GenericError';

/**
 * Error used when another state is expected.
 */
export class IllegalStateError extends GenericError {}
