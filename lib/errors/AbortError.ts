import { GenericError } from './GenericError';

/**
 * Error used to abort a process.
 * When this error is thrown the user has already been notified about the error.
 * This error should never be logged or printed to the user.
 */
export class AbortError extends GenericError {
	public constructor() {
		super('Do not log or show this error');
	}
}
